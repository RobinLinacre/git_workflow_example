# Git workshop script

## Introduction
This session is to share the first iteration of the Git workflow that we 
have put together from the many available options that are suggested
online.

Iteration because we do not expect it to be perfect. Hopefully it is functional for now and with feedback from all we can improve it.

Considering the fact that as far as we know there is quite a variety of levels of understanding of Git here, we will go through the Workflow example we've prepared by doing it live so we can make sure we all reach a common level of Git expertise. 


## Overview
Let's forget about Git for a moment.  
The workflow goes like this:

  1. Alice starts a project locally.
  2. Alice creates a clone of that project in a remote location.
  3. Bob joins the project.
  4. Bob clones the remote version of the project to his local environment.
  5. Bob works locally on a feature on top of Alice's work, without tempering
  with it.
  6. Bob sends it to the remote location and asks for a revision by someone
  else (probably Alice? and maybe others)
  7. Alice (+ others) give a thumbs up/down to Bob's work.  
  8. (a) If all thumbs are up, Bob locally merges his work with the latest version in the
  remote location and makes that change available to everyone.  
  (b). If any thumb is down, Bob discusses with the person that wasn't happy
  with his work and either convinces her or fixes the concerns.

Git is the perfect tool to do this in a consistent and resilient manner.

## Tools
Git can be used in three different ways:

  - The command line / shell  
  - A Git Graphical User Interface
  - Plugins for specific IDEs or general purpose editors

The only **officially** (Git team) **supported** tool is the command line. As a
consequence this is the only tool that has full functionality. Also this is the
only tool that is the same **everywhere**, it's always available and allows a
common language wherever you go. And probably one of the most important things is that if you try to find **help online** it will almost always be a command line solution.
Therefore, a minimum knowledge of the Git command line is **necessary**, and that's
what I'll use today to go over the workflow.  

The other tools are an extra. Of course you can use whatever tool you like but
you cannot expect others to use the same tools you've chosen.   
As an example, Michael will show how he uses Git from the IntelliJ editor via a
plugin. There is a way to do that from R studio, and from most modern editors
and IDEs.


Before we start with an example, can everyone open a Terminal on the MacBook?
it doesn't matter if it's with _Terminal_ or with _iTerm_ (I'll use the
latter).  

## Workflow in practice
### Working locally
Let's say I'm starting a project, my own project with no collaboration.

I create a new directory with the name of the project.  
For now assume I'll put everything in here - structuring the project
directories is probably important but out of scope for today.

Before I start there's something I'll need to do once: tell Git who I am.

```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

This information is actually going to be stored in `~/.gitconfig`. There are at
least three Git configuration files: for all users, for all my projects, for my
work with a specific project. `~/.gitconfig` is the one just for me but for all my projects.

One useful thing to add to your `.gitconfig` file are aliases.

See mine where I have included some popular ones:

```
$ vim ~/.gitconfig
```

Now, we want to make this (`~/projects/git_demo/`) folder become a Git
repository. A repository is where you keep all your files **and** all the
history of the changes.

```
$ git init
```

Let's take a look at how the directory has changed:

```
$ ls -la
```

The `.git` directory is the actual repository that contains all the
information we're tracking.

Let's create something to put into it:

```
$ touch foo
```

`foo` is now on the _Working Directory_ but it is **not** on the _Repository_
yet.

To commit a change we have to go through a two step process (which can be
simplified but we'll talk about that later). We first `add` the file to the
_Staging Area_ and afterwards we actually make the change to the repository
with a `commit`.

Let's first check the status

```
$ git status
```

We can see `foo` has been detected by Git as a file that is ready to start
being tracked. So we add it to the staging area:

```
$ git add foo
$ git status
```
and now we decide we want to commit everything we have on the staging area,
i.e. we add the change to the Git repository.

```
$ git commit
$ git status
```

As you can see all commits require a message. This message is very important,
and probably the bit that will/should take more time. However, we're now only
working on our own so let's keep it simple.

Why do we need a message? The objective of tracking your changes with Git is two-fold:

  1. **Legibility of the history**: You want to be able to look at the log of the
  commits and understand what has happened to the code whilst you weren't
  looking.
  2. **Reason for changes**: The same way as in commenting code, it is particularly
  interesting to see why you did what you did (more than actually saying what
  you did which is usually obvious).

Our repository has now one commit!  
We can check the history of commits:

```
$ git log
```

Although this is informative, the format is (I believe) not very good, so making
use of .gitconfig aliases I tend to run the _nice log_ one with `nl` which
shows a nicer one-line summary of the commit.

```
$ git nl
```

Let's see what information I'm showing:

_Hash_: It's good to know that a commit is uniquely defined by its SHA1 40 digit
hash (usually enough with the first few characters which is what I'm showing
here).  
_When_ the change was committed to the repository.  
_Message_: The first line of the commit message.  
_Who_: The name of the person that created the commit.  
_HEAD_: This is the point of the repository that is shown on the _Working
Directory_.  
_Branches_: The different lines of commits we have locally.  

There are at least three dimensions that is likely you'll need to keep in mind
while working with Git:

  1. Working Directory / Staging area / Repository - difference between in and
  out of the repository.
  2. Branches - different lines of development
  3. Local / Remotes - difference between private and public*


Let's look at branching.  

As you probably know, one of the good aspects of Git is that branching is easy
and unexpensive.

The default branch is _master_

In our workflow we are suggesting the use of 2 main branches plus the feature
branches:

  - _master_: A branch with releases only
  - _development_: A branch that contains only stable versions but will usually
    be more advanced than _master_
  - _feature-i_: All the branches where we develop new features. Naming of the
    branches is free but should be meaningful of what the branch is trying to solve.

It's not fundamental to have both _master_ and _development_ for projects where
there is no live deployment expected. So even if for the rest of this workshop I'll
assume we have both, I won't be using _master_ at all. 

#### The most important rule of our workflow
**No one works directly on master or development**

Therefore, if I want to start working I must create a new branch:

```
$ git branch development  # assuming that originally only master was available
$ git branch feature1
$ git branch -v
$ git checkout feature1
```

There is a shortcut for this:

```
$ git checkout -b feature1
```
---
### EXERCISE 1

  1. Create a new Git repository.
  2. Write on a file called `theraven.txt`:  
  > Once upon a midnight dreary, while I pondered, weak and weary,  
  Over many a quaint and curious volume of forgotten lore.
  3. Commit the changes
  4. Create a branch
  5. Add a couple of lines:  
  > While I nodded, nearly napping, suddenly there came a tapping,  
    As of some one gently rapping, rapping at my chamber door.
  6. Commit the changes

---

If you now `checkout` _master_ you'll notice that when you inspect the file we
still have the original version.

```
$ git checkout master
```

`checkout` moves _HEAD_ to where you want, and therefore, it makes a particular
branch or point of the history visible on the _Working Directory_.

Let me quickly show how to move around the branches

```
$ git checkout <commit>
```
Will detach the HEAD and take it to the commit you specified. If you want a
specific branch to be set there or to be moved there:

```
$ git checkout -b feature2
```
or

```
$ git branch -f feature1 <commit>
```

Finally, to wrap up before sharing the work with our colleagues, we want to
make the history readable. This can be done with `$ git rebase --interactive`.  

Let me quickly add some extra commits to the history.
```
$ touch one
$ git commit -a -m "to squash"
$ touch two
$ git commit -a -m "to squash"
$ touch three
$ git commit -a -m "to squash"
```
and now rebase interactively

```
$ git rebase -i HEAD~<n>  # instead of <n> include the number of commits back
```

With this you'll be able to re-order commits, squash them, rewrite their
message, etc.

An important consideration is the fact that you **must not use rebase for
commits that have already been made public** (e.g. commits that you took from upstream).

---
### EXERCISE 2
  1. Make a few commits to _theraven.txt_
  2. Rebase interactively some of the commits with at last one `squash` and one
  `rewrite`.
  3. Check how your Git history has changed.

---

There is one step I've skipped but it's common practice and I personally think
we should include into our standard workflow. That is the use of `.gitignore`.
Not sure how familiar you are with it but in short it's a text file where you
can specify any files you don't want Git to track.

### Working with remotes
So far we're just at the first step of our workflow. Let's move to the second
one: 
 
>  Alice creates a clone of the project in a remote location

So let's do that.

We go to the browser and access GitLab. I'm assuming we all have a user there,
if you don't create one now.   

There are multiple ways we could handle the remote location of the
repositories on the GitLab server. I'm referring to the way we make sure
everyone that has the need to have access to a repository actually has access
to it and that when someone wants to search for a repository that is possible.

An interesting option is to use GitLab groups, I haven't used them much so we
will have to test their functionality for us, but it is our suggestion for now.

Let's move on, and for this session now let's use our own users.

Click on the _New Project_ upper right cross.  
Give it a (sensible) name.  
And decide what type of permissions you want to give to other users. Note that
you can change that later on.  

And proceed. You'll get the instructions on what to do next.

```
$ git remote add <address>.git
$ git push -u origin master
```

I've cheated a bit here.
Two protocols can be used when communicating with your remote repositories:
_http_ and _ssh_.

  - **http** doesn't need any set up and security is obtained by authenticating every
time you connect to the GitLab server.  
  - **ssh** requires some setting up, that is, creating ssh keys and putting your
public key on GitLab so it recognises you when sending a petition. The
instructions for setting it up are very well explained on GitLab (or GitHub)
so I suggest you give it a go if you haven't before.

_ssh_ is my choice of protocol in general because it's really annoying to be keying in
the password for every push. You can try that on your own later.
Note: on Dune/AWS the necessary port (22 by default)  is not open yet. I've asked for a change to ensure we can
work comfortably there too.

Now, notice you've just pushed the _master_ branch. So we need to push
_development_ and any _feature_ branches we're ready to make public.  

```
$ git checkout development
$ git push origin development
$ git checkout feature
$ git push origin feature
```

---
### EXERCISE 3
  1. If you never had, sign up to GitLab.
  2. Create a new project
  3. Push your local repo to GitLab (include _master_, _development_ and one
  _feature_).
---


### Working collaboratively
Now it's Bob's time!
> Bob joins the project  

This basically means that Alice gives Bob developer permissions to push
branches to the project.

And,
> Bob clones the repo  

Let's assume Alice has been working and she has the _poetry_ repository on GitLab.
She gives Bob the name of the repository and its location by whatever means she
prefers. Bob clones it.

```
$ git clone git@gitlab.com:xxxxx/poetry.git 
```

Now Bob has a new directory `poetry` in which he has his local version of the
_poetry_ repository.  
Initially the clone will be of _master_ only.  

Bob needs to pull the other ones we're interested in. These include at least
_development_ and maybe one of the _feature_ branches (e.g. _raven_).  

Usually a single person will be working on a branch and for now we will work
under that assumption.  On the other hand, it will happen very often that you
will want to pull a feature branch that someone else has developed to review/test it
and give your thumbs up/down

```
$ git checkout master
$ git fetch --all
$ git checkout -b development origin/development
$ git checkout -b raven origin/raven 
```

Now Bob works on something new let's say it's _rings_


```
$ git checkout -b _rings_
$ git add foo
$ git commit
...
$ git add bar
$ git commit
```

Before making it public we make it readable:

```
$ git rebase -i HEAD~10
```

Fix any conflicts against _development_

```
$ git checkout development
$ git pull origin development
$ git checkout rings 
$ git rebase development
```

We're almost ready to publish!

```
$ git push origin rings 
```

If the feature is ready we:

  - Add a _merge request_ on GitLab, and
  - Go to the Slack's #project channel and send a
message to a couple (?) of team members from which we would like their thumbs
up.

Let's assume we got the approval. i.e. point 7 on the workflow

Next we go to point 8.(a) that is we can merge with _development_

Depending on the activity level we might want to repeat the previous step in
case _development_ has changed since our last pull.

The merge must happen locally. In principle any _destructive_ action should be
done locally.

```
$ git checkout development
$ git merge --no-ff rings 
```

All *merges must be no-fastforward*. This is a matter of
taste but we believe it makes the history cleaner and easier to read.

At this point Bob's work is published and we just need some cleaning up.

Delete the working branch on your local repository, and push the that change to
the remote branch (effectively deleting it there).

```
$ git branch -d rings 
$ git pull --rebase=preserve origin development
$ git push origin development
$ git push origin :rings
```

Alice and other users using that branch will need to get the latest status by:
```
$ git checkout development
$ git fetch —all
$ git remote prune origin
```

---
### EXERCISE 4 - feature end to end process
  1. clone the _poetry_ repository from: https://gitlab.com/xxxx/poetry.git
  2. add something (e.g. a poem) to the repository  
  3. submit a merge request to two of your colleagues
  4. get approval from two people in the room
  5. merge your work
  6. publish it
  7. clean up

---


## Other guidelines

This is definitely one of the best sets of recommendations I've found:

https://sethrobertson.github.io/GitBestPractices/

