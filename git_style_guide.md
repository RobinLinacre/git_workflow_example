# Git Style Guide

## This document

The guidelines included here are strong
suggestions and should be followed as much as possible because:

  - They have been built trying to follow the community standards, therefore
    if you adopt them they will be useful to you almost anywhere you go.
  - They will make life easier to you, your team mates, future contributors or
    third parties that want to follow the progress of the project.

However, this is not meant to be a completely static document. If any of these
guidelines is
agreed to be unuseful or a new guideline is deemed necessary, the document must
adapt. We would however, expect that the document changes are small after a
first stage of tunning.

The initial recommendations steal a lot from the references mentioned at the
end. If you modify this document please include the references in which you're
basing the amendments.

## Guidelines 

### Commit message
A good commit message briefly summarises the "what" for scanning purposes, but
also includes the "why". If the "what" in the message isn't enough, the diff is
there as a fallback. This isn't true for the "why" of a change - this can be
much harder or impossible to reconstruct, but is often of great significance.

Each commit message consists of a **header**, a **body** and a **footer**.  The
header has a special
format that includes a **type** and a **subject**:

```
<type>: <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The first line: _type_ and _subject_ are mandatory, whereas _body_ and _footer_
are optional. Note that the _BLANK LINES_ are mandatory if _body_ or _footer_
are present.

Any line of the commit message cannot be longer than 80 characters! This allows the
message to be easier
to read on github as well as in various git tools.

#### type

Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space,
formatting, missing semi-colons, etc)
* **refactor**: A code change that neither fixes a bug or adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such
* as documentation
  generation

#### subject

The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

#### body

Just as in the **subject**, use the imperative, present tense: "change" not
"changed" nor "changes"
The body should include the motivation for the change and contrast this with
previous behavior.

#### footer

The footer should contain any information about *Breaking Changes* and is
also the place to
reference GitHub issues that this commit *Closes*.

*Breaking Changes* are detected as such if the footer contains a line
starting with BREAKING CHANGE:
(with optional newlines) The rest of the commit message is then used for this.

Based on
https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#commit


### Commit frequency

You should commit very often. It's always easier to _squash_ two commits that
have a similar purpose than to split one commit in two.

### Tag names

We will follow the [SemVer](http://semver.org) versioning convention: 

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
>    MAJOR version when you make incompatible API changes,  
>    MINOR version when you add functionality in a backwards-compatible
>        manner, and  
>    PATCH version when you make backwards-compatible bug fixes.
>            
>    Additional labels for pre-release and build metadata are available
>    as extensions to the MAJOR.MINOR.PATCH format. 

### Branch size

Branches (other than master and development) must be short and as much as possible they should have a single
purpose. 

### Branch names

The name of the branch should refer to its purpose. Do not include your name on
the branch name (that's already recored by Git). 

### Destructive actions

Keep any destructive actions local. The most common destructive actions are:
_merge_ and _rebase_. 




## References

https://github.com/alphagov/styleguides/blob/master/git.md

https://github.com/conventional-changelog/conventional-changelog/blob/a5505865ff3dd710cf757f50530e73ef0ca641da/conventions/angular.md

https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message

https://github.com/torvalds/linux/pull/17#issuecomment-5659933

http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

http://semver.org

https://git-scm.com/book/ch5-2.html
